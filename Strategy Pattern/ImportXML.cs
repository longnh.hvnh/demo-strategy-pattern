﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Pattern
{
    public class ImportXML : ImportMethod
    {
        public string Import()
        {
            return "This is Import XML";
        }
    }
}
