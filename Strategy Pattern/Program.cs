﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var importMethod = new ImportImplementation(new ImportExcel());

            Console.WriteLine(importMethod.GetImportMethod());

            Console.ReadLine();
        }
    }
}
